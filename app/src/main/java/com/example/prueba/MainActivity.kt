package com.example.prueba

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults.buttonColors
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.prueba.ui.theme.PruebaTheme
import androidx.compose.ui.res.painterResource as painterResource1
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PruebaTheme {
                Surface(modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background) {
                    Column(
                        verticalArrangement=Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.padding(100.dp)
                    ) {
                    }
                }
            }
        }
    }
}
@Composable
fun Pruebabotones() {
    Column(verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()) {
        var num by remember {
            mutableStateOf(0)
        }
        var flor1 by remember {
            mutableStateOf(R.drawable.flor1)
        }
        var flor2 by remember {
            mutableStateOf(R.drawable.flor2)
        }
        var flor3 by remember {
            mutableStateOf(R.drawable.flor3)
        }
        var cont by remember {
            mutableStateOf(1)
        }
        var cont2 by remember {
            mutableStateOf(0)
        }
        Column(Modifier.padding(15.dp)) {
            if (cont == 1)
                Image(
                    painter = painterResource1(id = flor1),
                    contentDescription = "huerto",
                    modifier=Modifier.padding(11.dp)
                )
            if (cont == 2) {
                Image(
                    painter = painterResource1(id = flor2),
                    contentDescription = "huerto",
                    modifier=Modifier.padding(12.dp)
                )
            }
            if (cont == 3) {
                Image(
                    painter = painterResource1(id = flor3),
                    contentDescription = "huerto"
                )
            }
        }
        Text(
            "Has ganado " + num.toString() + "€",textAlign = TextAlign.Center
        )
        Text(
            "Has dado click " + cont2 + " veces",textAlign = TextAlign.Center
        )
        Button(
            onClick = {
                for (x in 1..1) {
                    cont++;
                    if (cont == 1) {
                        flor1 = R.drawable.flor1
                    }
                    if (cont == 2) {
                        flor2 = R.drawable.flor2
                    }
                    if (cont == 3) {
                        flor3 = R.drawable.flor3
                    }
                    if (cont >= 4) {
                        num += (1..5).random()
                        cont = 1;
                    }
                    cont2++
                }
            }, colors = buttonColors(Color.Black, Color.White)
        )
        {
            Text(
                text = "presiona para que crezca" ,textAlign = TextAlign.Center
            )
        }
        Button(onClick = {
            num = 0;
            cont = 1;
            cont2=0;
        }) {
            Text(text = "Reiniciar",textAlign = TextAlign.Center)
        }
    }
}
    @Preview(showBackground = true)
    @Composable
    fun PreviewBotones() {
        PruebaTheme {
            Pruebabotones()
        }
    }